@extends('template.content')
@section('container')
{{-- @dd($data); --}}
<div class="container position-absolute top-50 start-50 translate-middle">
    <div class="card text-dark bg-info mb-3" style="max-width: 18rem;">
        <div class="card-header"> {{$data->nama}} </div>
        <div class="card-body">
        <h5 class="card-title">
            @if ($data->verifikasi)
            <td><span class="badge bg-primary">Verified</span></td>
            @else
            <td><span class="badge bg-danger">UnVerified</span></td>
            @endif </h5>
        <p class="card-text">{{$data->nik}}</p>
        </div>
    </div>
</div>
@endsection