@extends('template.content')
@section('container')
<div class="container p-5">
    <form action="{{route('data.update' , $data->id)}}" method="POST">
        @csrf
        @method('PUT')
        <div class="mt-5 mb-3">
            <label for="nik" class="form-label">NIK</label>
            <input type="text" class="form-control" id="nik" name="nik" value="{{$data->nik}}">
          </div>
          <div class="mb-3">
            <label for="nokk" class="form-label">NOKK</label>
            <input type="text" class="form-control" id="nokk" name="nokk" value="{{$data->nokk}}">
          </div>
          <div class="mb-3">
            <label for="nama" class="form-label">NAMA</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}">
          </div>
          <div class="mb-3">
            <label for="verifikasi" class="form-label">STATUS</label>
            <input type="number" class="form-control" id="verifikasi" name="verifikasi" value="{{$data->verifikasi}}">
          </div>
          <div class="d-grid gap-2">
          <button type="submit" class="btn btn-primary mb-2">Simpan</button>
          </div>
    </form>
    <form method="POST" action="{{ route('data.delete', $data->id) }}" id="hapus">
        @csrf
        @method('DELETE')
        <div class="d-grid gap-2">
        <button type="submit" class="btn btn-danger" onclick="return confirm('yakin?')">Hapus</button>
        </div>
    </form> 
</div>
@endsection