@extends('template.content')
@section('container')    

<div class="container p-4">
    <h3>
        <a href="{{route('data.store')}}">Tambah Data</a>
    </h3>
    <div class="row">
   
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th scope="col">NAMA</th>
          <th scope="col">NIK</th>
          <th scope="col">NOKK</th>
          <th scope="col">STATUS</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($data as $item)     
        <tr>
          @if ($item->verifikasi == 1)     
          <td><a href="{{route('data.update', $item->id)}}">{{$item->nama}}</a></td>
          <td>{{$item->nik}}</td>
          <td>{{$item->nokk}}</td>
          @if ($item->verifikasi)
          <td><span class="badge bg-primary">Verified</span></td>
          @else
          <td><span class="badge bg-danger">UnVerified</span></td>
          @endif
          @else
          @auth
          <td><a href="{{route('data.update', $item->id)}}">{{$item->nama}}</a></td>
          <td>{{$item->nik}}</td>
          <td>{{$item->nokk}}</td>
          @if ($item->verifikasi)
          <td><span class="badge bg-primary">Verified</span></td>
          @else
          <td><span class="badge bg-danger">UnVerified</span></td>
          @endif 
          @endauth
          @endif
        </tr>
        @endforeach
      </tbody>
</div>
</div>

@endsection
