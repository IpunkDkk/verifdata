@extends('template.content')
@section('container')
<div class="container p-5">
    <form action="{{route('data.store')}}" method="POST">
        @csrf
        <div class="mt-5 mb-3">
            <label for="nik" class="form-label">NIK</label>
            <input type="text" class="form-control" id="nik" name="nik">
          </div>
          <div class="mb-3">
            <label for="nokk" class="form-label">NOKK</label>
            <input type="text" class="form-control" id="nokk" name="nokk">
          </div>
          <div class="mb-3">
            <label for="nama" class="form-label">NAMA</label>
            <input type="text" class="form-control" id="nama" name="nama">
          </div>
          <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
</div>
@endsection