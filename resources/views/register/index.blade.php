<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Signin</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
<link href="{{asset('/css/bootstrap.min.css')}}" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="{{asset('signin.css')}}" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-registration">
  <form action="{{route('register')}}" method="POST">
    @csrf
    <h1 class="h3 mb-3 fw-normal">Registration Form</h1>

    <div class="form-floating">
      <input type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name " placeholder="name" required value="{{old('name')}}">
      <label for="name">Name</label>
      @error('name')
          <div class="invalid-feedback">
              {{$message}}
          </div>
      @enderror
    </div>
    <div class="form-floating">
        <input type="text" name="username" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="username" required value="{{old('username')}}">
        <label for="username">Username</label>
        @error('username')
          <div class="invalid-feedback">
              {{$message}}
          </div>
        @enderror
      </div>
    <div class="form-floating">
        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="floatingInput" placeholder="name@example.com" required value="{{old('email')}}">
        <label for="floatingInput">Email address</label>
        @error('email')
          <div class="invalid-feedback">
              {{$message}}
          </div>
        @enderror
      </div>
    <div class="form-floating">
      <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" required>
      <label for="password">Password</label>
      @error('password')
          <div class="invalid-feedback">
              {{$message}}
          </div>
      @enderror
    </div>
    <button class="w-100 btn btn-lg btn-primary" type="submit">Sign up</button>
    <small>Already Registered? <a href="{{route('login')}}">login</a></small>
  </form>
</main>


    
  </body>
</html>
