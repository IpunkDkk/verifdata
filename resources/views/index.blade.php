@extends('template.content')
@section('container')


   <div class="container position-absolute top-50 start-50 translate-middle">
     <center>
       <div class="container">
         @auth
         <h1 class="display-3">Halo Staf {{auth()->user()->username}}</h1>
        @else
        <h1 class="display-3">Halo Guest</h1>
         @endauth
         <p>Tools Ini Dibuat Agar Lebih mempermudah Submit Data</p>
         <p><a class="btn btn-primary btn-lg" href="{{route('data.store')}}" role="button">Tambah Data &raquo;</a></p>
       </div>
     </center>
    </div>
       
   @endsection