<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Verifikasi Data</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
</head>
<body>
    <div class="container">
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            <div class="container">
                <a class="navbar-brand" href="/">Verifikasi Data</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/">Admin</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="{{route('data.all')}}">guest</a>
                    </li>
                </ul>
                <ul class="navbar-nav ms-auto">
                    @auth
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                          Welcome Back, {{auth()->user()->username}}
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
                          <li>
                              <form action="{{route('logout')}}" method="POST">
                                  @csrf
                                  <button type="submit" class="dropdown-item">Logout <i class="bi bi-box-arrow-right"></i></button>
                              </form>
                            </li>
                        </ul>
                      </li>
                    @else
                    <div class="d-flex me-md-3">
                        <a class="btn btn-outline-primary" href="{{route('login')}}">Login</i></a>
                    </div>
                    @endauth
                </ul>
                </div>
            </div>
            </nav>
        </header>
    </div>