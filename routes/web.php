<?php

use App\Http\Controllers\DataController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

//basehome
Route::get('/' , function(){
    return view('index');
})->name('home');

// Route::get('/login', function() {
//     return view('login');
// });
Route::prefix('login')->group(function(){
    Route::get('/' , [LoginController::class, 'index'])->name('login')->middleware('guest');
    Route::post('/' , [LoginController::class, 'authenticate'])->name('login');
    Route::post('/logout' , [LoginController::class, 'logout'])->name('logout');
});

route::prefix('register')->group(function(){
    Route::get('/' , [RegisterController::class, 'index'])->name('register');
    Route::post('/' , [RegisterController::class, 'store'])->name('register');

});

Route::prefix('data')->group(function(){
    Route::get('/' , [DataController::class, 'index'])->name('data.all');
    Route::get('/store' , [DataController::class, 'create'])->name('data.store')->middleware('auth');
    Route::post('/store' , [DataController::class, 'store'])->name('data.store')->middleware('auth');
    Route::get('/show/{id}' , [DataController::class, 'show'])->name('data.show')->middleware('auth');
    Route::get('/update/{id}' , [DataController::class, 'edit'])->name('data.update')->middleware('auth');
    Route::put('/update/{id}' , [DataController::class, 'update'])->name('data.update')->middleware('auth');
    Route::delete('/delete/{id}' , [DataController::class, 'destroy'])->name('data.delete')->middleware('auth');
});